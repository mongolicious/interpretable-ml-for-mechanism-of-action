package edu.cmu.mohimanilab;

import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.Iterator;

public class InputIter implements Iterator<Path> {
    private FileSystem fs;
    private Scanner reader;
    private String dbDir;

    public InputIter(String dbDir, InputStream inputStream) throws IOException {
        fs = FileSystems.getDefault();
        this.dbDir = dbDir;
        reader = new Scanner(inputStream);
    }

    public boolean hasNext() {
        return reader.hasNextLine();
    }

    public Path next() {
        String nextLine = reader.nextLine().trim();
        return (dbDir != null) ? fs.getPath(dbDir, nextLine) : fs.getPath(nextLine);
    }
}

