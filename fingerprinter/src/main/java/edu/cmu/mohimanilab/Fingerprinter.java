package edu.cmu.mohimanilab;

import java.lang.InstantiationException;
import java.lang.IllegalAccessException;
import java.lang.NoSuchMethodException;
import java.lang.reflect.InvocationTargetException;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.charset.StandardCharsets;
import java.util.BitSet;
import org.openscience.cdk.io.MDLV3000Reader;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.fingerprint.KlekotaRothFingerprinter;
import org.openscience.cdk.fingerprint.IFingerprinter;
import org.openscience.cdk.fingerprint.PubchemFingerprinter;

public class Fingerprinter {
    public static <T extends IFingerprinter> String getFingerprint(Class<T> fpClass, Path molPath) throws CDKException, IOException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        IAtomContainer mol = buildMol(molPath);
        PubchemFingerprinter pubchemFp = new PubchemFingerprinter(DefaultChemObjectBuilder.getInstance());;
        T fingerprinter = null; 
        if (fpClass.isInstance(pubchemFp)) {
            fingerprinter = (T) pubchemFp;
        } else {
            fingerprinter = fpClass.getDeclaredConstructor().newInstance(); 
        }
        BitSet fp = fingerprinter.getFingerprint(mol);
        return fpToString(fp, fingerprinter.getSize());
    }

    private static IAtomContainer buildMol(Path molPath) throws CDKException, IOException {
        BufferedReader reader = Files.newBufferedReader(molPath, StandardCharsets.UTF_8);
        return new MDLV3000Reader(reader).readMolecule(DefaultChemObjectBuilder.getInstance());
    }

    private static String fpToString(BitSet fp, int numBits) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < numBits; i++) {
            if (i >= fp.length()) {
                result.append('0');
                continue;
            }
            result.append(fp.get(i) ? '1' : '0');
        }

        return result.toString();
    }
}
