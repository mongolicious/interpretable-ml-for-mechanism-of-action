package edu.cmu.mohimanilab;

import java.lang.InstantiationException;
import java.lang.IllegalAccessException;
import java.lang.ClassNotFoundException;
import java.util.ArrayList;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import java.nio.file.Path;
import java.io.FileReader;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.io.InputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.BitSet;
import java.util.HashMap;
import org.openscience.cdk.io.MDLV3000Reader;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.fingerprint.IFingerprinter;

public class App 
{
    public static void main( String[] args ) throws CDKException, IOException, InstantiationException, IllegalAccessException
    {
        Cli cli = new Cli(args);
        String fpTypeString = (String) cli.getOpt("f");
        Class fpType = null;
        try {
            fpType = Class.forName("org.openscience.cdk.fingerprint." + fpTypeString);
        } catch (ClassNotFoundException e) {
            System.err.println("Invalid fingerprinter class " + fpTypeString);
            System.exit(1);
        }
        Class fpTypeEffectivelyFinal = fpType;

        InputStream inStream = System.in;
        String inputFile = (String) cli.getOpt("i");
        if (inputFile != null) {
            inStream = new FileInputStream(inputFile);
        }

        InputIter in = new InputIter((String) cli.getOpt("d"), inStream);
        Stream<Path> inputStream = StreamSupport.stream(
            Spliterators.spliteratorUnknownSize(in, Spliterator.ORDERED),
            true
        );

        String result = getFingerprints(inputStream, fpTypeEffectivelyFinal);

        String outFile = (String) cli.getOpt("o");
        PrintStream outStream = System.out;
        if (outFile != null) {
            outStream = new PrintStream(new File(outFile));
        }
        outStream.println(result);
    }

    private static String getFingerprints(Stream<Path> inputStream, Class fpType) {
        ArrayList<String> results = new ArrayList();
        ResultComputer rc = new ResultComputer(fpType);
        return inputStream.map((path) -> rc.getFingerprint(path)).collect(Collectors.joining("\n"));
    }

    // private static String getFingerprint(Path path) {
    //     String result = null;
    //     try {
    //         result = Fingerprinter.getFingerprint(fpTypeEffectivelyFinal, path);
    //     } catch (Exception e) {
    //         System.err.println("Failure on file " + path + ". Will output empty line");
    //         e.printStackTrace();
    //         result = "";
    //     }
    //     return result;
    // }

    private static class ResultComputer {
        private Class fpType;

        public ResultComputer(Class fpType) {
            this.fpType = fpType;
        }

        public String getFingerprint(Path path) {
            String result = null;
            try {
                result = Fingerprinter.getFingerprint(this.fpType, path);
            } catch (Exception e) {
                System.err.println("Failure on file " + path + ". Will output empty line");
                e.printStackTrace();
                result = "";
            }
            return result;
        }
    }
}
