package edu.cmu.mohimanilab;

import org.apache.commons.cli.*;

public class Cli {
    private Options options;
    private DefaultParser parser;
    private HelpFormatter formatter;
    private CommandLine cmd;

    public Cli(String[] args) {
        options = new Options();
        initOptions();
        parser = new DefaultParser();
        formatter = new HelpFormatter();
        parseOptions(args);
    }

    private void initOptions() {
        Option help = Option.builder("h")
            .longOpt("help")
            .desc("Print this message")
            .build();
        options.addOption(help);

        Option dbDir = Option.builder("d")
            .longOpt("dbDir")
            .hasArg()
            .type(String.class)
            .desc("Base directory for database")
            .build();
        options.addOption(dbDir);

        Option inputFile = Option.builder("i")
            .longOpt("input")
            .hasArg()
            .type(String.class)
            .desc("Path to input file containing paths to MOL files (default: STDIN)")
            .build();
        options.addOption(inputFile);

        Option outputFile = Option.builder("o")
            .longOpt("output")
            .hasArg()
            .type(String.class)
            .desc("Path to file in which to save fingerprints (default: STDOUT)")
            .build();
        options.addOption(outputFile);

        Option fpType = Option.builder("f")
            .longOpt("fingerprint")
            .required(true)
            .hasArg(true)
            .desc("Class name of CDK fingerprinter to use")
            .type(String.class)
            .build();
        options.addOption(fpType);
    }

    private void parseOptions(String[] args) {
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            formatter.printHelp("fingerprinter", options);

            System.exit(1);
        }

        if (cmd.hasOption("h")) {
            formatter.printHelp("fingerprinter", options);
            System.exit(0);
        }
    }

    public Object getOpt(String opt) {
        Object result = null;
        try {
            result = cmd.getParsedOptionValue(opt);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            formatter.printHelp("fingerprinter", options);
            System.exit(1);
        }

        return result;
    }
}
