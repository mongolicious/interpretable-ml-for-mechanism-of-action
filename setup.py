from setuptools import setup, find_packages

setup(
    name="int-act-pred",
    version="0.1.0",
    description="Methods for interpretable antibiotic activity prediction",
    package_data={"int_act_pred": ["py.typed"]},
    package_dir={"": "src"},
    packages=find_packages("src"),
)
