# Development Setup
## Create conda environment
If not installed follow instructions to install miniconda from [here](https://docs.conda.io/en/latest/miniconda.html).
Afterwards, from the root directory of this repository, run
```bash
conda env create -f environment.yaml
```
This will create the `activity_pred` conda environment containing:
- python3
- [rdkit](http://www.rdkit.org/docs/GettingStartedInPython.html)
- numpy
- [sklearn](https://scikit-learn.org/stable/user_guide.html)
- pandas
- ipython
- jupyter
- [black](https://black.readthedocs.io/en/stable/)
- [mypy](http://mypy-lang.org/)
- [flake8](https://flake8.pycqa.org/en/latest/)

Be sure to run `conda activate activity_pred` to load these dependencies in before development.

## Install package locally
Existing python code is a package called `int_act_pred`. After create the conda environment the package can be install in it as follows.

```bash
conda actviate activity_pred
pip install -e .
```

Now we can run `import int_act_pred` and access all the existing functions.


## ./examples/
Contains two ipython notebooks corresponding to research in paper. Interpretable ML Classifier accurate as Stokes et al is the notebook corresponding to dataset from Stokes et al. Sparse Logistic Ring Method On Dose InhibitionData-Commented .ipynb is notebook corresponding to CO-ADD dataset. 



