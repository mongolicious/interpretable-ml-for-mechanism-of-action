from rdkit import Chem
from typing import List


def rings(mol: Chem.Mol) -> List[Chem.Mol]:
    """
    Extracts ring substructures from provided molecule. See rdkit.Chem.GetSymmSSSR.

    :param mol: rdkit molecule to process
    :return: a list of molecules containing simple rings in molecule
    """
    substructs = []
    for ring in Chem.GetSymmSSSR(mol):
        # atoms in ring
        ring = list(ring)
        # convert to tuples of atoms that should be bonded
        ring = list(zip(ring, ring[1:])) + [(ring[-1], ring[0])]
        bonds = [mol.GetBondBetweenAtoms(*i).GetIdx() for i in ring]
        substructs.append(Chem.PathToSubmol(mol, bonds, useQuery=True))
    return substructs
