import pandas as pd
import numpy as np

from .types import SKLearnModel


def train_model(training: pd.DataFrame, model: SKLearnModel) -> None:
    """
    Trains provided sklearn model on the training data.
    Provided training dataset is expected to have:
        - an "X" column with a numpy array features for a data point
        - a "y" column with an integer label

    :param training: dataframe with X and y columns containing training data and labels
    :param model: a scikit-learn model that has a .fit() function
    """
    X = np.array(training["X"].to_list())
    y = np.array(training["y"].to_list())
    model.fit(X, y)


def test_model(testing: pd.DataFrame, model: SKLearnModel) -> float:
    """
    Tests provided sklearn model on the testing data.
    Provided testing dataset is expected to have:
        - an "X" column with a numpy array features for a data point
        - a "y" column with an integer label

    :param testing: dataframe with X and y columns containing testing data and labels
    :param model: a trained scikit-learn model that has a .score() function
    """
    X = np.array(testing["X"].to_list())
    y = np.array(testing["y"].to_list())
    return model.score(X, y)
