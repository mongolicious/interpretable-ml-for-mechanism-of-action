import pandas as pd
from rdkit import Chem


def read_training(path: str) -> pd.DataFrame:
    """
    Loads training data from CSV and returns a dataframe.

    :param path: path to CSV containing data from Table S1B
    :return: dataframe with "molecule", "name", and "activity" columns
    """
    df = pd.read_csv(path)
    # store rdkit molecule objects in column
    df["molecule"] = df.SMILES.apply(Chem.MolFromSmiles)
    df["activity"] = df.Activity.apply(lambda x: x.strip() == "Active")
    df["name"] = df.Name.str.lower()
    return df[["molecule", "name", "activity"]].copy()


def read_test(path: str) -> pd.DataFrame:
    """
    Loads test data from CSV and returns a dataframe.

    :param path: path to CSV containing data from Table S2F
    :return: dataframe with "molecule", "name", and "score" columns
    """
    df = pd.read_csv(path)
    df["molecule"] = df.SMILES.apply(Chem.MolFromSmiles)
    df["score"] = df.Pred_Score
    df["name"] = df.Name.str.lower()
    df = df.sort_values("score")
    return df[["molecule", "name", "score"]].copy()


def to_molfile(path: str, mol: Chem.Mol) -> None:
    """
    Converts a RDKit molecule into a V3000 MOL file.

    :param path: path to intended output file
    :param mol: RDKit molecule to be converted
    """
    Chem.MolToMolFile(mol, path, forceV3000=True)
