from typing import Protocol, Optional
import numpy as np


class SKLearnModel(Protocol):
    def fit(
        self, X: np.ndarray, y: np.ndarray, sample_weight: Optional[np.ndarray] = None
    ) -> "SKLearnModel":
        ...

    def predict(self, X: np.ndarray) -> np.ndarray:
        ...

    def score(
        self, X: np.ndarray, y: np.ndarray, sample_weight: Optional[np.ndarray] = None
    ) -> float:
        ...
