from search import all_hits, significance, count_hits, sig_subs, compute_combos
from utils import load_data
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report
import pickle
import os
from glob import glob
from typing import Tuple, Optional, List
from argparse import ArgumentParser, Namespace, ArgumentDefaultsHelpFormatter


def get_args() -> Namespace:
    parser = ArgumentParser(prog="moa", formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "--multi",
        default="multinomial",
        help="Type of multiclass logistic regression to use. Either 'ovr' or 'multinomial'",
    )
    parser.add_argument(
        "--cutoff",
        default=1e-10,
        type=float,
        help="Corrected p-value cutoff for significant substructures",
    )
    parser.add_argument(
        "--combos",
        default=False,
        action="store_true",
        help="Consider co-occurrence of two substructures",
    )
    parser.add_argument(
        "--min-moa-cnt",
        default=30,
        type=int,
        help="Minimum number of samples required to consider a MOA label",
    )
    parser.add_argument(
        "--use-uncommon",
        default=False,
        action="store_true",
        help="Add extra 'uncommon moa' label",
    )
    parser.add_argument(
        "--penalty",
        default="l2",
        type=str,
        help="Norm used in logistic regression penalization. One of 'l1', 'l2', 'elasticnet', or 'none'.",
    )
    parser.add_argument(
        "--clean",
        default=False,
        action="store_true",
        help="Remove all pickle cache files and exit",
    )

    args = parser.parse_args()
    if args.multi not in ("multinomial", "ovr"):
        parser.error(
            "Invalid value provided to --multi flag. Must be either 'ovr' or 'multinomial'"
        )
    if args.penalty not in ("l1", "l2", "elasticnet", "none"):
        parser.error(
            "Invalid value provided to --penalty flag. Must be one of 'l1', 'l2', 'elasticnet', or 'none'"
        )
    return args


def process_bioactivity(
    use_combos: bool, cache: bool = True
) -> Tuple[
    pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame, Optional[pd.DataFrame]
]:
    if cache and os.path.exists("train_cache.pkl"):
        with open("train_cache.pkl", "rb") as f:
            train_cache = pickle.load(f)
        train = train_cache["train"]
        hits = train_cache["hits"]
        rings = train_cache["rings"]
        funcs = train_cache["funcs"]
        combos = train_cache["combos"]

        return train, hits, rings, funcs, combos

    train = load_data("train_preprocess.csv")
    hits, rings, funcs = all_hits(train)
    combos = None
    if use_combos:
        combos = compute_combos(hits)
        tmp = combos.copy()
    rings, funcs, combos = count_hits(train, hits, rings, funcs, combos)
    if use_combos:
        hits = hits.merge(tmp, on="id")
    significance(train, rings, funcs, combos)

    if cache:
        with open("train_cache.pkl", "wb") as f:
            train_cache = {
                "train": train,
                "hits": hits,
                "rings": rings,
                "funcs": funcs,
                "combos": combos,
            }
            pickle.dump(train_cache, f, pickle.DEFAULT_PROTOCOL)

    return train, hits, rings, funcs, combos


def process_moa(
    rings: pd.DataFrame, combo_cols: Optional[List[str]], min_moa_cnt: int, cache: bool
) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    if cache and os.path.exists("test_cache.pkl"):
        with open("test_cache.pkl", "rb") as f:
            test_cache = pickle.load(f)
        test = test_cache["test"]
        test_hits = test_cache["test_hits"]
        common_moa = test_cache["common_moa"]
        return test, test_hits, common_moa

    test = load_data("test_preprocess.csv")
    test_hits, _, _ = all_hits(test, rings)
    if combo_cols is not None:
        combos = compute_combos(test_hits, combo_cols)
        test_hits = test_hits.merge(combos, on="id")
    moa_cnt = test.groupby("moa").count().id
    common_moa = test[test.moa.isin(moa_cnt[moa_cnt > min_moa_cnt].index)].index

    if cache:
        with open("test_cache.pkl", "wb") as f:
            test_cache = {
                "test": test,
                "test_hits": test_hits,
                "common_moa": common_moa,
            }
            pickle.dump(test_cache, f, pickle.DEFAULT_PROTOCOL)

    return test, test_hits, common_moa


def main() -> int:
    args = get_args()
    if args.clean:
        print("Cleaning cache")
        for filename in glob("*.pkl"):
            os.remove(filename)
        return 0

    print("Processing bioactivity data")
    train, hits, rings, funcs, combos = process_bioactivity(
        use_combos=args.combos, cache=True
    )

    print("Processing drug repurposing hub data")
    combo_cols = list(combos.index) if args.combos else None
    test, test_hits, common_moa = process_moa(
        rings, combo_cols=combo_cols, min_moa_cnt=args.min_moa_cnt, cache=True
    )

    structs = [rings, funcs, combos if args.combos else None]
    sig_rings, sig_funcs, sig_combos = [
        i[i.corrected_p_val <= args.cutoff].sort_values("corrected_p_val")
        if i is not None
        else None
        for i in structs
    ]
    sig_cols = list(sig_rings.id) + list(sig_funcs.id)
    if sig_combos is not None:
        sig_cols += list(sig_combos.id)
    with open("sig_cols.pkl", "wb") as f:
        pickle.dump(sig_cols, f, pickle.DEFAULT_PROTOCOL)
    print("Number of siginificant structures:", len(sig_cols))
    print(sig_cols)

    print("Fitting model")
    clf = LogisticRegression(
        multi_class=args.multi, solver="saga", penalty=args.penalty
    )
    if args.use_uncommon:
        X = test_hits.loc[:, sig_cols].values
        y = test.moa.copy()
        y[~y.index.isin(common_moa)] = "uncommon moa"
        y = y.values
    else:
        X = test_hits.loc[common_moa, sig_cols].values
        y = test.moa[common_moa].values
    clf.fit(X, y)
    np.save("weights.npy", clf.coef_)
    with open("classifier.pkl", "wb") as f:
        pickle.dump(clf, f, pickle.DEFAULT_PROTOCOL)
    report = pd.DataFrame(classification_report(y, clf.predict(X), zero_division=0, output_dict=True)).transpose()
    report.to_csv("report.csv")
    # print(classification_report(y, clf.predict(X), zero_division=0))

    return 0


if __name__ == "__main__":
    main()
