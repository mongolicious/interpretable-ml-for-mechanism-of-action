import pandas as pd
from rdkit import Chem
from typing import Iterable, List

def sanitize(mol: Chem.rdchem.Mol):
    """
    Sanitizes molecule in place but does not enforce valence

    :param mol: Unsanitized molecule to be sanitized
    :type mol: Chem.rdchem.Mol
    :return: Nothing, molecule sanitized in place
    :rtype: None
    """
    san_flags = Chem.SANITIZE_SYMMRINGS|Chem.SANITIZE_SETCONJUGATION|Chem.SANITIZE_SETHYBRIDIZATION
    Chem.SanitizeMol(mol, san_flags)

def jaccard(a: Iterable, b: Iterable) -> float:
    """
    Computes Jaccard similarity between two iterables

    :param a: First iterable
    :param b: Second iterable
    :type a: Iterable where elements support comparison
    :type b: Iterable where elements support comparison
    :return: Jaccard similarity between a and b
    :rtype: float
    """
    a, b = set(a), set(b)
    return len(a.intersection(b)) / len(a.union(b))

def ring_substruct(mol: Chem.rdchem.Mol) -> List[Chem.rdchem.Mol]:
    """
    Extracts ring substructures from molecule.

    :param mol: Molecule to extract substructures from
    :type mol: Chem.rdchem.Mol
    :return: A list of sub-molecules corresponding to substructures
    :rtype: List[Chem.rdchem.Mol]
    """
    substructs = []

    # extract ring featuresx
    for ring in Chem.GetSymmSSSR(mol):
        ring = list(ring)
        ring += [ring[0]]
        ring_bonds = [mol.GetBondBetweenAtoms(i,j).GetIdx() for i,j in zip(ring, ring[1:])]
        substructs.append(Chem.PathToSubmol(mol, ring_bonds, useQuery=True))

    return substructs

def ring_substructs (df: pd.DataFrame, print_stats: bool = False) -> List[Chem.rdchem.Mol]:
    '''
    Extracts unique ring substructures from dataset

    :param df: Dataframe containing training dataset
    :param print_stats: Whether or not to print statistics about ring substructures
    :type df: pandas.DataFrame
    :type print_stats: bool
    :return: Unique ring substructures from dataset
    :rtype: List[Chem.rdchem.Mol]
   
    '''
    substructs = df.molecule.apply(ring_substruct).sum()
    if print_stats:
        print("Total # ring substructs:", len(substructs))

    # remove duplicates by finding unique canonical SMILES
    substructs = {Chem.MolToSmiles(i) for i in substructs}
    substructs = [Chem.MolFromSmiles(i, sanitize=False) for i in substructs]
    for i in substructs:
        sanitize(i)

    if print_stats:
        print("Unique # ring substructs:", len(substructs))

    substructs = pd.DataFrame({"mol": substructs})
    substructs["smiles"] = substructs.mol.apply(Chem.MolToSmiles)
    substructs["id"] = [f"RING{i:03}" for i in substructs.index]

    return substructs[["id", "smiles", "mol"]]

def load_data(filename: str) -> pd.DataFrame:
    """
    Loads data from CSV file and pre-processes it

    :param filename: Path to CSV file
    :type filename: str
    :return: Dataframe containing pre-processed data
    :rtype: pandas.DataFrame
    """
    df = pd.read_csv(filename)
    print (df)
    # all dataframes have smiles column, also compute mol object
    df["mol"] = df.smiles.apply(Chem.MolFromSmiles)
    return df
