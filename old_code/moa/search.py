from rdkit import Chem
from scipy import stats
import numpy as np
import pandas as pd
from typing import Callable, List, Tuple, Optional
from functools import reduce
from itertools import combinations

from functional_groups import functional_substructs
from utils import ring_substructs

def ring_search(df: pd.DataFrame, ring: Chem.rdchem.Mol) -> np.array:
    """
    Searches all molecules in a dataframe for target ring-based substructure.

    :param df: Dataframe to search against
    :param ring: Ring substructure to search for
    :type df: pandas.DataFrame
    :type ring: Chem.rdchem.Mol
    :return: Count table [[ # active hits    # active misses     ]
                          [ # inactive hits  # inactive misses   ]]
    :rtype: np.array
    """
    df = df.groupby(df.activity)
    # total count in each label
    activity = df.count().molecule.values

    # count of hits for each label
    search_f = lambda x: x.HasSubstructMatch(ring)
    hits = df.molecule.apply(lambda x: x.apply(search_f).sum()).values

    # reformat to match expected output
    return np.flip(np.vstack([hits, activity - hits]).T, 0)

def functional_search(df: pd.DataFrame, func: Callable[[Chem.rdchem.Mol], bool]) -> np.array:
    """
    Searches all molecules in a dataframe for target functional group substructure

    :param df: Dataframe to search against
    :param func: Function that determines presence of functional group
    :type df: pandas.DataFrame
    :type func: Callable[[Chem.rdchem.Mol], bool]
    :return: Count table [[ # active hits    # active misses     ]
                          [ # inactive hits  # inactive misses   ]]
    :rtype: np.array
    """
    df = df.groupby(df.activity)
    # total count in each label
    activity = df.count().molecule.values

    # count of hits for each label
    hits = df.molecule.apply(lambda x: x.apply(func).sum()).values

    # reformat to match expected output
    return np.flip(np.vstack([hits, activity - hits]).T, 0)

def ring_hits(df: pd.DataFrame, rings: pd.DataFrame) -> pd.DataFrame:
    """
    Computes binary hit vectors for each of the ring substructures

    :param df: Dataframe to search against
    :param rings: Ring substructures to search for
    :type df: pandas.DataFrame
    :type ring: pandas.DataFrame
    :return: Dataframe with binary vectors representing presence of substructures in each compound
    :rtype: pandas.DataFrame
    """
    search_f = lambda ring_row: df.molecule.apply(lambda mol: int(mol.HasSubstructMatch(ring_row["mol"])))
    out = rings.apply(search_f, axis=1).transpose().rename(dict(zip(range(len(rings)), rings.id)), axis=1)
    out = pd.concat([df.id, out], axis=1)
    return out

def functional_hits(df: pd.DataFrame, funcs: pd.DataFrame) -> pd.DataFrame:
    """
    Computes binary hit vectors for each of the functional group substructures

    :param df: Dataframe to search against
    :param funcs: Dataframe with functional substructures
    :type df: pandas.DataFrame
    :type funcs: pandas.DataFrame
    :return: Dataframe with binary vectors representing presence of substructures in each compound
    :rtype: pandas.DataFrame
    """
    
    search_f = lambda mol: funcs.func.apply(lambda f: int(f(mol)))
    out = df.molecule.apply(search_f).rename(dict(zip(range(len(funcs)), funcs.id)), axis=1)
    out = pd.concat([df.id, out], axis=1)
    return out

def all_hits(df: pd.DataFrame, rings: Optional[pd.DataFrame] = None) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    """
    Computes binary hit table for ring and functional substructures

    :param df: Dataframe to search against
    :param rings: Dataframe of ring substructures to use, if None computed from df parameter
    :type df: pandas.DataFrame
    :type rings: pandas.DataFrame or None
    :return: Hit table dataframe, dataframe of ring substructures, dataframe of functional substructures
    :rtype: Tuple[pandas.DataFrame, pandas.DataFrame, pandas.DataFrame]
    """
    if rings is None:
        rings = ring_substructs(df)
    r_hits = ring_hits(df, rings)
    funcs = functional_substructs()
    f_hits = functional_hits(df, funcs)
    return r_hits.merge(f_hits, on="id"), rings, funcs

def count_hits(df: pd.DataFrame,
               hits: pd.DataFrame,
               rings: pd.DataFrame,
               funcs: pd.DataFrame,
               combos: Optional[pd.DataFrame] = None) -> Tuple[pd.DataFrame, pd.DataFrame, Optional[pd.DataFrame]]:
    """
    Counts number of inactive and active hits for each substructure, updates information
    in corresponding substructure dataframe and return it

    :param df: Dataframe to search against
    :param hits: Dataframe first output of all_hits()
    :param rings: Dataframe with ring substructures
    :param funcs: Dataframe with functional group substructures
    :param use_combos: Consider co-occurence of substructures as well
    :type df: pandas.DataFrame
    :type hits: pandas.DataFrame
    :type rings: pandas.DataFrame
    :type funcs: pandas.DataFrame
    :return: New ring, functional group dataframes, and possibly combo dataframes
    :rtype: Tuple[pandas.DataFrame, pandas.DataFrame, Optional[pandas.DataFrame]]
    """
    # combine hit and raw training data
    df = df.merge(hits, on="id")
    if combos is not None:
        df = df.merge(combos, on="id")
    g = df.groupby(df.activity)

    # get names of substructure columns
    ring_cols = [i for i in df.columns if i.startswith("RING")]
    func_cols = [i for i in df.columns if i.startswith("FUNC")]
    sub_cols = ring_cols + func_cols
    ring_cols = [i for i in ring_cols if not "_" in i]
    func_cols = [i for i in func_cols if not "_" in i]

    # only sum counts for substructure columns
    sum_f = lambda x: x.sum() if x.name in sub_cols else 0
    sub_counts = g.apply(lambda x: x.apply(sum_f))[sub_cols].transpose().rename({True: "active_hits", False: "inactive_hits"}, axis=1)
    sub_counts["id"] = sub_counts.index
    ring_hit_df = rings.merge(sub_counts.loc[ring_cols], on="id")
    func_hit_df = funcs.merge(sub_counts.loc[func_cols], on="id")

    combo_hit_df = None
    if combos is not None:
        combo_hit_df = sub_counts.loc[list(set(sub_cols) - set(ring_cols).union(set(func_cols)))]

    return ring_hit_df, func_hit_df, combo_hit_df

def compute_combos(hits: pd.DataFrame, ids: Optional[List[str]] = None) -> pd.DataFrame:
    """
    Computes hit dataframe for co-occurring substructures.

    :param hits: Dataframe of ring and functional group hits
    :param ids: List of combination substructure IDs to keep, if blank non-zero ones are kept
    :type hits: pandas.DataFrame
    :type ids: List[str] or None
    :return: Dataframe of combined substructure hits
    :rtype: pandas.DataFrame
    """
    combos = {}
    sub_cols = [i for i in hits.columns if i != "id"]
    for i,j in combinations(sub_cols, 2):
        curr_id = "{}_{}".format(*sorted([i,j]))
        combos[curr_id] = np.logical_and(hits[i], hits[j]).astype(int)
    combos = pd.DataFrame(combos)

    if not ids is None:
        combos = combos[ids]
    else:
        # remove combinations with no overlap at all
        combos = combos.transpose()[combos.sum(axis=0) > 0].transpose()
    combos["id"] = hits.id
    return combos

def significance(df: pd.DataFrame, rings: pd.DataFrame, funcs: pd.DataFrame, combos: Optional[pd.DataFrame] = None) -> None:
    """
    Adds p-value and corrected p-value columns to the ring and functional group
    substructure dataframes

    :param df: Dataframe to search against
    :param rings: Dataframe with ring substructures and count columns
    :param funcs: Dataframe with functional group substructures and count columns
    :param combos: Dataframe with combinations of 2 substructures and count columns
    :type df: pandas.DataFrame
    :type rings: pandas.DataFrame
    :type funcs: pandas.DataFrame
    :type combos: pandas.DataFrame
    :return: Nothing
    :rtype: None
    """
    total = df.groupby("activity").count().id.rename({False: "f", True: "t"})[["t", "f"]].values
    table_f = lambda x: np.vstack([x.values, total - x.values]).T
    hypotheses = (0 if (rings is None) else len(rings)) + (0 if (funcs is None) else len(funcs))
    if not combos is None:
        hypotheses += len(combos)

    # add ring p-values
    tables = rings[["active_hits", "inactive_hits"]].apply(table_f, axis=1)
    rings["p_val"] = tables.apply(stats.fisher_exact).apply(lambda x: x[1])
    rings["corrected_p_val"] = rings.p_val * hypotheses

    # add functional p-values
    if not funcs is None:
        tables = funcs[["active_hits", "inactive_hits"]].apply(table_f, axis=1)
        funcs["p_val"] = tables.apply(stats.fisher_exact).apply(lambda x: x[1])
        funcs["corrected_p_val"] = funcs.p_val * hypotheses

    # add combo p-values
    if not combos is None:
        tables = combos[["active_hits", "inactive_hits"]].apply(table_f, axis=1)
        combos["p_val"] = tables.apply(stats.fisher_exact).apply(lambda x: x[1])
        combos["corrected_p_val"] = combos.p_val * hypotheses

def sig_subs(cutoff: float, rings: pd.DataFrame, funcs: pd.DataFrame) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Computes statistically significant ring and functional substructures according
    to provided p-value cutoff.

    :param cutoff: Cutoff for p-val, p-vals less than this considered significant
    :param rings: Dataframe of ring substructures containing significance information
    :param funcs: Dataframe of functional group substructures containing significance information
    :type cutoff: float
    :type rings: pandas.DataFrame
    :type funcs: pandas.DataFrame
    :return: Dataframe of significant rings, dataframe of significant functional groups
    :rtype: Tuple[pandas.DataFrame, pandas.DataFrame]
    """
    sig_rings = None if (rings is None) else rings[rings.corrected_p_val <= cutoff].sort_values("corrected_p_val")     
    sig_funcs = None if (funcs is None) else funcs[funcs.corrected_p_val <= cutoff].sort_values("corrected_p_val")
    return sig_rings, sig_funcs

# TODO: make tanimoto network
