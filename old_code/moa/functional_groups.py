from rdkit import Chem
from rdkit.Chem import Descriptors
from typing import Dict, Callable, List
import pandas as pd

## Begin Oxygen Groups

def hydroxyl(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has hydroxyl groups, not including carboxyl groups.
    Uses rdkit.Chem.Descriptors.fr_Al_OH and rdkit.Chem.Descriptors.fr_Ar_OH
    https://en.wikipedia.org/wiki/Hydroxy_group

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a hydroxyl group
    :rtype: bool
    """
    return Descriptors.fr_Al_OH(mol) + Descriptors.fr_Ar_OH(mol) > 0

def carboxyl(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has carboxyl groups.
    Uses rdkit.Chem.Descriptors.fr_COO
    https://en.wikipedia.org/wiki/Carboxylic_acid

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a carboxyl group
    :rtype: bool
    """
    return Descriptors.fr_COO(mol) > 0

def carbonyl(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has carbonyl groups that aren't a part of a carboxyl group.
    Uses rdkit.Chem.Descriptors.fr_ketone and rdkit.Chem.Descriptors.fr_aldehyde
    https://en.wikipedia.org/wiki/Carbonyl_group

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a carbonyl group
    :rtype: bool
    """
    return Descriptors.fr_ketone(mol) + Descriptors.fr_aldehyde(mol) > 0

def oxime(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has oxime groups.
    Uses rdkit.Chem.Descriptors.fr_oxime
    https://en.wikipedia.org/wiki/Oxime

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a oxime group
    :rtype: bool
    """
    return Descriptors.fr_oxime(mol) > 0

def ester(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has ester groups.
    Uses rdkit.Chem.Descriptors.fr_ester
    https://en.wikipedia.org/wiki/Ester

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a ester group
    :rtype: bool
    """
    return Descriptors.fr_ester(mol) > 0

def ether(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has ether groups.
    Uses rdkit.Chem.Descriptors.fr_ether
    https://en.wikipedia.org/wiki/Ether

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a ether group
    :rtype: bool
    """
    return Descriptors.fr_ether(mol) > 0

## End Oxygen Groups

## Begin Nitrogen Groups

def amine(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has amine groups.
    Excludes amines where the nitrogen is in a ring.
    https://en.wikipedia.org/wiki/Amine

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a amine group
    :rtype: bool
    """
    return mol.HasSubstructMatch(Chem.MolFromSmarts("[#7&!R&H0,#7&!R&H1,#7&!R&H2]"))

def imine(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has imine groups.
    Uses rdkit.Chem.Descriptors.fr_Imine
    https://en.wikipedia.org/wiki/Imine

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a imine group
    :rtype: bool
    """
    return Descriptors.fr_Imine(mol) > 0

def nitrile(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has nitrile groups.
    Uses rdkit.Chem.Descriptors.fr_nitrile
    https://en.wikipedia.org/wiki/Nitrile

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a nitrile group
    :rtype: bool
    """
    return Descriptors.fr_nitrile(mol) > 0

def hydrazine(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has hydrazine groups.
    Uses rdkit.Chem.Descriptors.fr_hdrzine

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a hydrazine group
    :rtype: bool
    """
    return Descriptors.fr_hdrzine(mol) > 0

def hydrazone(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has hydrazone groups.
    Uses rdkit.Chem.Descriptors.fr_hdrzone

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a hydrazone group
    :rtype: bool
    """
    return Descriptors.fr_hdrzone(mol) > 0

def nitroso(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has nitroso groups.
    Uses rdkit.Chem.Descriptors.fr_nitroso

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a nitroso group
    :rtype: bool
    """
    return Descriptors.fr_nitroso(mol) > 0

def hydroxylamine(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has hydroxylamine groups.
    Uses rdkit.Chem.Descriptors.fr_N_O

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a hydroxylamine group
    :rtype: bool
    """
    return Descriptors.fr_N_O(mol) > 0

def nitro(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has nitro groups.
    Uses rdkit.Chem.Descriptors.fr_nitro
    https://en.wikipedia.org/wiki/Nitro_compound

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a nitro group
    :rtype: bool
    """
    return Descriptors.fr_nitro(mol) > 0

def azo(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has azo, diazo, or azide groups.
    Uses rdkit.Chem.Descriptors.fr_azo and rdkit.Chem.Descriptors.fr_diazo

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a azo or diazo group
    :rtype: bool
    """
    return Descriptors.fr_azo(mol) + Descriptors.fr_diazo(mol) > 0

def azide(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has azide groups.
    Uses rdkit.Chem.Descriptors.fr_azide

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a azide group
    :rtype: bool
    """
    return Descriptors.fr_azide(mol) > 0

def amide(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has amide groups.
    Uses rdkit.Chem.Descriptors.fr_amide
    https://en.wikipedia.org/wiki/Amide

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a amide group
    :rtype: bool
    """
    return Descriptors.fr_amide(mol) > 0

def amidine(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has amidine groups.
    Uses rdkit.Chem.Descriptors.fr_amidine
    https://en.wikipedia.org/wiki/Amidine

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a amidine group
    :rtype: bool
    """
    return Descriptors.fr_amidine(mol) > 0

def guanidine(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has guanidine groups.
    Uses rdkit.Chem.Descriptors.fr_guanido
    https://en.wikipedia.org/wiki/Guanidine

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a guanidine group
    :rtype: bool
    """
    return Descriptors.fr_guanido(mol) > 0

def imide(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has imide groups.
    Uses rdkit.Chem.Descriptors.fr_imide
    https://en.wikipedia.org/wiki/Imide

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a imide group
    :rtype: bool
    """
    return Descriptors.fr_imide(mol) > 0

def isocyanate(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has isocyanate groups.
    Uses rdkit.Chem.Descriptors.fr_isocyan
    https://en.wikipedia.org/wiki/Isocyanate

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a isocyanate group
    :rtype: bool
    """
    return Descriptors.fr_isocyan(mol) > 0

def isothiocyanate(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has isothiocyanates groups.
    Uses rdkit.Chem.Descriptors.fr_isothiocyan
    https://en.wikipedia.org/wiki/Isothiocyanate

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a isothiocyanates group
    :rtype: bool
    """
    return Descriptors.fr_isothiocyan(mol) > 0

def thiocyanate(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has thiocyanate groups.
    Uses rdkit.Chem.Descriptors.fr_thiocyan
    https://en.wikipedia.org/wiki/Thiocyanate

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a thiocyanate group
    :rtype: bool
    """
    return Descriptors.fr_thiocyan(mol) > 0

## End Nitrogen Groups

## Begin Halogen Groups

def haloalkane(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule is a haloalkane
    Uses rdkit.Chem.Descriptors.fr_alkyl_halide
    https://en.wikipedia.org/wiki/Haloalkane

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule is a haloalkane
    :rtype: bool
    """
    return Descriptors.fr_alkyl_halide(mol) > 0

## End Halogen Groups

## Begin Sulfur Groups

def thioether(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has non-cyclic thioether groups.
    Non-cyclic here means that the sulfur atom is not in a ring.
    https://en.wikipedia.org/wiki/Sulfide

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a thioether group
    :rtype: bool
    """
    return mol.HasSubstructMatch(Chem.MolFromSmarts("[SX2&!R](-[#6])-[#6]"))

def thiol(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has thiol groups.
    Uses rdkit.Chem.Descriptors.fr_SH

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a thiol group
    :rtype: bool
    """
    return Descriptors.fr_SH(mol) > 0

def thiocarbonyl(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has thiocarbonyl groups.
    Uses rdkit.Chem.Descriptors.fr_C_S
    https://en.wikipedia.org/wiki/Thioketone

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a thiocarbonyl group
    :rtype: bool
    """
    return Descriptors.fr_C_S(mol) > 0

def sulfone(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has sulfone groups.
    Uses rdkit.Chem.Descriptors.fr_sulfone
    https://en.wikipedia.org/wiki/Sulfone

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a sulfone group
    :rtype: bool
    """
    return Descriptors.fr_sulfone(mol) > 0

def sulfonamide(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has sulfonamide groups.
    Uses rdkit.Chem.Descriptors.fr_sulfonamd
    https://en.wikipedia.org/wiki/Sulfonamide

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a sulfonamide group
    :rtype: bool
    """
    return Descriptors.fr_sulfonamd(mol) > 0

## End Sulfur Groups

## Begin Misc Groups

def carbamide(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has urea groups.
    Uses rdkit.Chem.Descriptors.fr_urea
    https://en.wikipedia.org/wiki/Urea

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a urea group
    :rtype: bool
    """
    return Descriptors.fr_urea(mol) > 0

## End Misc Groups

## Begin Phosphate Groups

def phosphoric_acid(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has phosphoric acid groups.
    Uses rdkit.Chem.Descriptors.fr_phos_acid
    https://en.wikipedia.org/wiki/Phosphoric_acid

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a phosphoric acid group
    :rtype: bool
    """
    return Descriptors.fr_phos_acid(mol) > 0

def phosphoric_ester(mol: Chem.rdchem.Mol) -> bool:
    """
    Checks if molecule has phosphoric ester groups.
    Uses rdkit.Chem.Descriptors.fr_phos_ester
    https://en.wikipedia.org/wiki/Ester#Inorganic_esters

    :param mol: Molecule to check
    :type mol: Chem.rdchem.Mol
    :return: Whether or not molecule contains a phosphoric ester group
    :rtype: bool
    """
    return Descriptors.fr_phos_ester(mol) > 0

## End Phosphate Groups

def functional_substructs() -> Dict[str, List[Callable[[Chem.rdchem.Mol], bool]]]:
    """
    Returns a mapping of functional group types to boolean returning
    functions that determine their presence in a molecule.

    :return: Dictionary collecting functional group functions into their broad types
    :rtype: Dict[str, List[Callable[[rdkit.Chem.rdkit.Mol], bool]]]
    """
    df = pd.DataFrame(
        [
            # oxygen groups
            { "func": hydroxyl,         "smarts": ["[#6]-[O]-[H]"],                                                 "group": "oxygen" },
            { "func": carboxyl,         "smarts": ["[#6]-C(=O)-[O]-[H]"],                                           "group": "oxygen" },
            { "func": carbonyl,         "smarts": ["[#6]-[CX3](=O)-[#6]", "[CX3](-[H])(=O)-[#6]"],                  "group": "oxygen" },
            { "func": oxime,            "smarts": ["[X]-[CX3](-[H])=[NX2]-[OX2]-[H]"],                              "group": "oxygen" },
            { "func": ester,            "smarts": ["[#6]-[CX3](=O)-[OX2H0]-[#6]"],                                  "group": "oxygen" },
            { "func": ether,            "smarts": ["[OD2](-[#6])-[#6]"],                                            "group": "oxygen" },
            # nitrogen groups
            { "func": amine,            "smarts": ["[X]-[N](-[H])-[H]", "[X]-[N](-[H])-[X]", "[X]-[N](-[X])-[X]"],  "group": "nitrogen" },
            { "func": imine,            "smarts": ["[Nv3](=C)-[#6]"],                                               "group": "nitrogen" },
            { "func": nitrile,          "smarts": ["[NX1]#[CX2]"],                                                  "group": "nitrogen" },
            { "func": hydrazine,        "smarts": ["[NX3]-[NX3]"],                                                  "group": "nitrogen" },
            { "func": hydrazone,        "smarts": ["C=N-[NX3]"],                                                    "group": "nitrogen" },
            { "func": nitroso,          "smarts": ["[#6]-[N]=O"],                                                   "group": "nitrogen" },
            { "func": hydroxylamine,    "smarts": ["[N](-O(-[H]))(-[X])-[C]"],                                      "group": "nitrogen" },
            { "func": nitro,            "smarts": ["[C]-[N](-[O])=[O]"],                                            "group": "nitrogen" },
            { "func": azo,              "smarts": ["[#6]-N=N-[#6]", "[N+]#N"],                                      "group": "nitrogen" },
            { "func": azide,            "smarts": ["[NX2-]-[NX2+]#[NX1]", "[NX2]=[NX2+]=[NX1-]"],                   "group": "nitrogen" },
            { "func": amide,            "smarts": ["[X]-C(=O)-N"],                                                  "group": "nitrogen" },
            { "func": amidine,          "smarts": ["C(=N)(-N)-[X]"],                                                "group": "nitrogen" },
            { "func": guanidine,        "smarts": ["C(=N)(-[N])-[N]"],                                              "group": "nitrogen" },
            { "func": imide,            "smarts": ["N(-C(=O))-C=O"],                                                "group": "nitrogen" },
            { "func": isocyanate,       "smarts": ["[X]-N=C=O"],                                                    "group": "nitrogen" },
            { "func": isothiocyanate,   "smarts": ["[X]-N=C=S"],                                                    "group": "nitrogen" },
            { "func": thiocyanate,      "smarts": ["[X]-S-C#N"],                                                    "group": "nitrogen" },
            # halogen groups
            { "func": haloalkane,       "smarts": ["C-[Cl]", "C-[Br]", "C-[I]", "C-[F]"],                           "group": "halogen" },
            # sulfur groups
            { "func": thioether,        "smarts": ["[SX2](-[#6])-C"],                                               "group": "sulfur" },
            { "func": thiol,            "smarts": ["[X]-[S]-[H]"],                                                  "group": "sulfur" },
            { "func": thiocarbonyl,     "smarts": ["C=[SX1]"],                                                      "group": "sulfur" },
            { "func": sulfone,          "smarts": ["S(=O)(=O)(-[#6])-[#6]"],                                        "group": "sulfur" },
            { "func": sulfonamide,      "smarts": ["N-S(=O)(=O)-[#6]"],                                             "group": "sulfur" },
            # misc groups
            { "func": carbamide,        "smarts": ["C(=O)(-N)-N"],                                                  "group": "misc" },
            # phosphate groups
            { "func": phosphoric_acid,  "smarts": ["P([OX1])([OX2H])([OX2H])[OX2H]"],                               "group": "phosphate" },
            { "func": phosphoric_ester, "smarts": ["P(=[OX1])(-[OX2]-[X])(-[O]-[X])-[O]-[X]"],                      "group": "phosphate" },
        ]
    )
    df["name"] = df.func.apply(lambda x: x.__name__)
    df["id"] = [f"FUNC{i:03}" for i in df.index]
    df["type"] = df["group"]
    return df[["id", "name", "smarts", "func", "type"]]
